import QtQuick 2.4
import Ubuntu.Components 1.3
import Qt.labs.settings 1.0
import Qt.labs.platform 1.0 //for StandardPaths
import io.thp.pyotherside 1.4 //for Python
import QtGraphicalEffects 1.0 //for RectangularGlow

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'themeswitch.danfro'

    property bool isCacheView: false

    Page {
        id: mainPage
        anchors.fill: parent
        property string themeFile: "file:///home/phablet/.config/ubuntu-ui-toolkit/theme.ini"
        property string wasTheme: parseFileContent(mainPage.fileContent)
        property string fileContent: openFile()

        //initiate a python component for calls to python commands
        Python {
            id: py

            Component.onCompleted: {
                //this works as the import statement in a python script
                importModule('shutil', function() { console.log("DEBUG: python loaded"); });
            }
        }

        header: PageHeader {
            id: header
            title: 'ThemeSwitch'


            Label {
              id: versionText
              anchors {right: parent.right; rightMargin: units.gu(6); verticalCenter: parent.verticalCenter}
              font.italic: true
              font.weight: Font.Light
              fontSize: "normal"
              text: "v%1".arg(Qt.application.version)
            }

            trailingActionBar {
                actions: [
                    Action {
                        id: cacheButton
                        iconName: "delete"
                        objectName: "clearCache"
                        text: i18n.tr("Clear app's qml cache")
                        onTriggered: {
                          isCacheView = !isCacheView;
                          toggleCacheView();
                        }
                    }
                ]
                objectName: "locationsTrailingActionBar"
            }
        }

        //rect layout for clear cache content
        Rectangle {
            id: cacheRect
            visible: false
            color: theme.palette.normal.background

            anchors.fill: parent
            anchors.topMargin: units.gu(6)

            Text {
              id: headerLabel
              anchors.top: parent.top
              anchors.topMargin: units.gu(2)
              anchors.horizontalCenter: parent.horizontalCenter
              wrapMode: Text.WordWrap
              font.bold: true
              color: theme.palette.normal.baseText
              text: i18n.tr("Delete cache?")
            }

            Text {
              id: descriptionLabel
              anchors.top: headerLabel.bottom
              anchors.topMargin: units.gu(2)
              anchors.horizontalCenter: parent.horizontalCenter
              wrapMode: Text.WordWrap
              color: theme.palette.normal.baseText
              text: i18n.tr("Press 'Delete cache' to clear the app's qml cache.")
            }

            Button {
              id: clearButton
              anchors.top: descriptionLabel.bottom
              anchors.topMargin: units.gu(2)
              anchors.horizontalCenter: parent.horizontalCenter
              color: theme.palette.normal.negative
              text: i18n.tr("Delete cache")

              onClicked: {
                  //delete the app's qmlcache folder using the python shutils module
                  py.call('shutil.rmtree', [StandardPaths.writableLocation(StandardPaths.CacheLocation).toString().replace("file://","") + "/qmlcache/"], function (result) {
                      // TODO: add error handling
                  });
                  isCacheView = !isCacheView;
                  toggleCacheView();
              }
            }

            Button {
                id: cancelButton
                anchors.top: clearButton.bottom
                anchors.topMargin: units.gu(2)
                anchors.horizontalCenter: parent.horizontalCenter
                text: i18n.tr("Cancel")
                onClicked: {
                    isCacheView = !isCacheView;
                    toggleCacheView();
                }
            }
        }

        //top column to show current theme and descriptive labels
        Column {
            id: topColumn
            spacing: units.gu(2)

            anchors {
                top: header.bottom
                topMargin: units.gu(2)
                horizontalCenter: parent.horizontalCenter
            }

            Label {
                id: descriptionLabelTheme
                anchors.horizontalCenter: parent.horizontalCenter
                text: i18n.tr("Current theme") + ": <i>%1</i>.".arg(parseFileContent(mainPage.fileContent))
                font.bold: true
                horizontalAlignment:  Text.AlignHCenter
            }

            Column {
                id: labelColumn
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: units.gu(0.5)
                Label {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("Select a new theme and")
                    horizontalAlignment: Text.AlignHCenter
                }
                Label {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr('restart all apps to apply the changes.')
                    font.italic: true
                    horizontalAlignment: Text.AlignHCenter
                    color: theme.palette.normal.negative
                }
            }
        }

        //flickable with the buttons to select the new theme
        Flickable {
            id: themePageFlickable
            clip: true
            flickableDirection: Flickable.AutoFlickIfNeeded

            anchors.fill: parent
            anchors.topMargin: header.height + topColumn.height + units.gu(4)
            anchors.bottomMargin: bottomColumn.height + units.gu(4)

            contentHeight: buttonCol.childrenRect.height + units.gu(3)

            Column {
                id: buttonCol
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: units.gu(2)
                anchors.top: parent.top
                spacing: units.gu(3.5)

                Button{
                    id: ambianceButton
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "Ambiance"
                    font.bold: true
                    visible: mainPage.wasTheme === "Ambiance" ? false : true
                    color: "#F7F7F7" //Porcelain
                    height: units.gu(8)
                    width: gradientButton.width*1.5
                    onClicked: {
                        switchTheme("Ambiance");
                        if (settings.quit == true) {
                            quitLabel.visible = true;
                            quitTimer.start();
                            themePageFlickable.visible = false;
                        }
                    }

                    RectangularGlow {
                        id: effectAmbiance
                        glowRadius: units.gu(2)
                        spread: 0.15
                        color: "#CDCDCD" //Silk
                        cornerRadius: 25
                        width: parent.width
                        height: parent.height
                        z: -1
                    }
                }

                Button{
                    id: suruDarkButton
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "SuruDark"
                    font.bold: true
                    visible: mainPage.wasTheme === "SuruDark" ? false : true
                    color: "#3B3B3B" //Inkstone
                    height: units.gu(8)
                    width: gradientButton.width*1.5
                    onClicked: {
                        switchTheme("SuruDark");
                        if (settings.quit == true) {
                            quitLabel.visible = true;
                            quitTimer.start();
                            themePageFlickable.visible = false;
                        }
                    }
                    RectangularGlow {
                        id: effectSuruDark
                        glowRadius: units.gu(2)
                        spread: 0.15
                        color: "#666666" //Graphite
                        cornerRadius: 25
                        width: parent.width
                        height: parent.height
                        z: -1
                    }
                }

                Column {
                    id: gradientCol
                    spacing: units.gu(0.5)
                    anchors.horizontalCenter: parent.horizontalCenter

                    Button{
                        id: gradientButton
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: "SuruGradient *"
                        font.pointSize: units.gu(1.25)
                        visible: mainPage.wasTheme === "SuruGradient" ? false : true
                        color: "#77216F" //Light aubergine
                        height: units.gu(3)
                        onClicked: {
                            switchTheme("SuruGradient");
                            if (settings.quit == true) {
                                quitLabel.visible = true;
                                quitTimer.start();
                                themePageFlickable.visible = false;
                            }
                        }
                    }

                    Label {
                        id: gradientWarningLabel
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.topMargin: -units.gu(1)
                        visible: mainPage.wasTheme === "SuruGradient" ? false : true
                        text: "<i>" + i18n.tr('* theme not supported by many apps') + "</i>"
                        horizontalAlignment: Text.AlignHCenter
                        font.pointSize: units.gu(1)
                    }
                }
            }
        }

        //bottom column with close setting and issue report link
        Column {
            id: bottomColumn
            spacing: units.gu(2)

            anchors {
                bottom: parent.bottom
                bottomMargin: units.gu(2)
                horizontalCenter: parent.horizontalCenter
            }

            Row {
                id: closeSetting
                anchors {
                  horizontalCenter: parent.horizontalCenter
                }
                spacing: units.gu(1.25)
                CheckBox {
                    id: quitSwitch
                    checked: settings.quit
                    onCheckedChanged: {
                        settings.quit = checked;
                    }
                }

                Label {
                    text: i18n.tr("Close app on theme selection?")
                    horizontalAlignment:  Text.AlignHCenter
                }
            }

            Row {
                id: issueLink
                anchors.horizontalCenter: parent.horizontalCenter

                Label {
                    id: bugreport
                    text: i18n.tr("Report an issue: ")
                }
                Label {
                    id: buglink
                    text: i18n.tr("ThemeSwitch on GitLab")
                    color: theme.palette.normal.activity
                    MouseArea {
                        anchors.fill: parent
                        onClicked: Qt.openUrlExternally('https://gitlab.com/Danfro/themeswitch/issues')
                    }
                }
            }
        }

        Label {
            id: quitLabel
            anchors.centerIn: parent
            visible: false
            textSize: Label.Large
            text: i18n.tr("Closing app ...")
        }
    }

    Timer {
        //the r/w process of XMLHttpRequest() does take a small time
        //so we allow a small delay after changing the theme before the next action
        id: quitTimer
        interval: 350 //0.5 second
        running: false
        repeat: false
        onTriggered: Qt.quit();
    }

    Settings {
        id: settings
        property bool quit: true
    }

    //changes the old and the new theme
    function switchTheme(themeToSet) {
        //make sure we only proceed if a valid theme is going to be set
        if (themeToSet == "SuruDark" || themeToSet == "Ambiance" || themeToSet == "SuruGradient") {
            console.log("theme was: " + mainPage.wasTheme)
            console.log("set theme to: " + themeToSet)
            mainPage.fileContent = mainPage.fileContent.replace(mainPage.wasTheme,themeToSet);
            saveFile();
            mainPage.wasTheme = themeToSet;
        }
    }

    //switches between clear cache view and set theme view
    function toggleCacheView() {
        cacheRect.visible = isCacheView

        themePageFlickable.visible = !isCacheView
        topColumn.visible = !isCacheView
        bottomColumn.visible = !isCacheView
    }

    //read the existing content of theme.ini
    function openFile() {
        var request = new XMLHttpRequest();
        request.open("GET",mainPage.themeFile , false);
        request.send(null);
        return request.responseText;
    }

    //extract the current theme name from theme.ini file content
    function parseFileContent(content) {
        //the returned file content should look like this:
        //[General]
        //theme= Ubuntu.Components.Themes.SuruDark (or.Ambiance)
        //empty line

        //get only second line and get theme name by stripping all noncharacter strings
        return content.split("\n")[1].split(".")[3].replace(/[\W_]+/g,"");
    }

    //write the content to theme.ini
    function saveFile() {
        var request = new XMLHttpRequest();
        request.open("PUT", mainPage.themeFile, false);
        //the file content is read on opening the app
        //the theme name is replaced in switchTheme function
        request.send(mainPage.fileContent);
        return request.status;
    }
}
