# *ThemeSwitch*

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/themeswitch.danfro)

A [UBports](https://ubports.com) app to quickly switch between the Ambiance and SuruDark system themes.

Just start the app and tap/click on "Change theme" to restart Unity8 with the new theme.
If you need to save some work first, tap/click "Close app" and change the theme later.

## This app runs unconfined!

It is using shell commands to change the theme by editing the '~/.config/ubuntu-ui-toolkit/theme.ini'. This file can not be accessed with confinement. Shell commands can not be issued without confinement.

The Python command "shutils.rmtree(.cache/themeswitch.danfro/qmlcache)" is used to delete the app's qml cache. This folder then gets recreated on app startup automatically if deleted.

## Screenshot

<img src="screenshot.png" alt="screenshot" width="220"/>

P.S. I do know that [UT Tweak Tool](https://gitlab.com/myii/ut-tweak-tool) can change system theme as well - **but** not as fast as *ThemeSwitch*! :-)
